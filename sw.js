self.addEventListener('install', e => {
    e.waitUntil(
        // self.skipWaiting(),
        caches.open('static').then(cache => {
            return cache.addAll([
                './',
                // assets
                    // fonts
                './assets/fonts/GiveYouGlory-Regular.ttf',
                './assets/fonts/Roboto-Regular.ttf',
                './assets/fonts/Roboto-Medium.ttf',
                    // sounds
                './assets/audio/Afrikaans.mp3',
                './assets/audio/Bemba.mp3',
                './assets/audio/click.mp3',
                './assets/audio/correct.mp3',
                './assets/audio/Deutsch.mp3',
                './assets/audio/English.mp3',
                './assets/audio/general.mp3',
                './assets/audio/incorrect.mp3',
                './assets/audio/isiXhosa.mp3',
                './assets/audio/isiZulu.mp3',
                './assets/audio/Russian.mp3',
                './assets/audio/SetSwana.mp3',
                './assets/audio/Shona.mp3',
                './assets/audio/soundcheck.mp3',
                './assets/audio/tralala.mp3',
                './assets/audio/Turkish.mp3',
                './assets/audio/unlock.mp3',
                    // images
                './assets/images/general/paper.jpg',
                './assets/images/general/underscore.png',
                './assets/images/icons/correct.svg',
                './assets/images/icons/huh.svg',
                './assets/images/icons/incorrect.svg',
                './assets/images/icons/ear.png',
                './assets/images/icons/ex.png',
                './assets/images/icons/question.png',
                './assets/images/icons/repeat-icon.png',
                './assets/images/icons/tick.png',
                './assets/images/logos/favicon.png',
                './assets/images/logos/splash-icon.png',
                './assets/images/logos/goethe-institute-logo.svg',
                './assets/images/minions/afrikaans.png',
                './assets/images/minions/bemba.png',
                './assets/images/minions/english.png',
                './assets/images/minions/german.png',
                './assets/images/minions/isizulu.png',
                './assets/images/minions/russian.png',
                './assets/images/minions/setswana.png',
                './assets/images/minions/shona.png',
                './assets/images/minions/turkish.png',
                './assets/images/minions/xhosa.png',
                './assets/images/results/astronaut.png',
                './assets/images/results/cold-water.png',
                './assets/images/results/hiking.png',
                './assets/images/results/piano.png',
                './assets/images/results/pie.png',
                // styles
                './style/css/style.css',
                './style/bootstrap/bootstrap.min.css',
                './style/animate/animate.min.css',
                // scripts
                './script/buttons.js',
                './script/constants.js',
                './script/difficulty.js',
                './script/gameAudio.js',
                './script/gameplay.js',
                './script/loading.js',
                './script/reset.js',
                './script/results.js',
                './script/rules.js'
            ])
        })
        .then(self.skipWaiting()) // forces installation of new sw
        .catch(function (error) {
            console.log(error);
        })
        
    )
})

self.addEventListener('fetch', e => {
    e.respondWith(
        caches.match(e.request).then( response => {
           return response || fetch(e.request) 
        })
    )
})

