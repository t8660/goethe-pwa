
let skip = 0
let watchAudioTime

skipAudioBtn.addEventListener('click', () => {
    skip++
    if(skip === 1){
        skipText.innerText = 'Skip'
    } else if(skip === 2){
        container02Part1.classList.add('d-none')
        container02Part2.classList.remove('d-none')
        englishAudio.pause()
        englishAudio.currentTime = 0
        rulesProgressBar.style.width = 0 + '%'
        skipAudioBtn.classList.remove('active')
        skipText.innerText = 'Heard it already?'
        skip = 0
        clearInterval(watchAudioTime)
    }
    setTimeout(() => {
        if(skip === 1){
            revertSkip()
        }
    }, 5000)
})

function revertSkip() {
    skipAudioBtn.classList.remove('active')
    skipText.innerText = 'Heard it already?'
    skip = 0
}

placeholderBtn.addEventListener('click', () => {
    hiddenAudioEls.forEach(item => {item.classList.remove('d-none')})
    playBtnDiv.classList.remove('animate__bounceInUp','animate__delay-2s')
    playBtnDiv.classList.add('animate__bounceOut')
    placeholderBtn.classList.remove('animate__animated','animate__pulse', 'animate__delay-1s', 'animate__infinite')
    
    beginIntro()
})

function beginIntro() {
    englishAudio = document.querySelector('#english')
    englishAudio.play()
    englishAudio.addEventListener('timeupdate', (e) => {
        let total = e.target.duration
        let current = e.target.currentTime
        rulesProgressBar.style.width = Math.round(current/total*100) + '%'
    })
    englishAudio.addEventListener('ended', () => {
        container02Part1.classList.add('d-none')
        container02Part2.classList.remove('d-none')
        englishAudio.currentTime = 0
    })
}

