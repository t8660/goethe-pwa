function setupAudio(audioId) {
    gameAudio = document.querySelector(`#${audioId.toLowerCase()}`)
    gameAudio.currentTime = 0;
    gameAudio.play()
    gameAudio.playbackRate = 5
    gameAudioListener()
    gameProgressBar.style.width = 0;
    spans.forEach(span => {span.classList.add('playing')})
}

function gameAudioListener() {
    if(gameAudio){
        gameAudio.addEventListener('playing', (e) => {audioPlaying(e)})
        gameAudio.addEventListener('ended', (e) => {audioEnded(e)})
    }
}

function audioPlaying(e) {
    if(gameAudio){
        watchGameAudioTime = setInterval(() => {
            let total = e.target.duration
            let current = gameAudio.currentTime
            gameProgressBar.style.width = Math.round(current/total*100) + '%'
            console.log('watchGameAudioTime')
        }, 500)
    }
}

function audioEnded (e) {
    spans.forEach(span => {span.classList.remove('playing')})
    audioBox.classList.add('d-none')
    gameplayBox.classList.remove('d-none')
    timerBox.classList.remove('d-none')
    timerBox.classList.add('d-flex')
    audioTerminate()
    // roundClock()
}

function audioTerminate() {
    gameAudio.pause()
    gameAudio.removeEventListener('playing', audioPlaying)
    gameAudio.removeEventListener('ended', audioEnded)
    clearInterval(watchGameAudioTime)
}