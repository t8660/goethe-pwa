'use strict';

diffOptions.forEach(option => {
    option.addEventListener('click', () => {
        let sound = document.querySelector(`#general`)
        sound.currentTime = 0;
        sound.play()
        if(option.id === 'easy'){difficulty = 3}
        else if (option.id === 'medium'){difficulty = 6}
        else {difficulty = 9} 
        diffOptions.forEach(item => {item.classList.remove('difficulty-selected')})
        option.classList.add('difficulty-selected')
        diffGoBtn.classList.remove('d-none')
    })
})