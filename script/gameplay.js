'use strict';

// begin game play for the first time
// 1. reset timer
// 2. begin a new round
function beginGamePlay() {
        setTimeout(() => {
            remainingTime = 15
            newRound(getShuffledElements(difficulty)) 
        }, 250) 
}
 
// establish the round answer by
// 1. shuffle the array
// 2. filter the shuffled array to exclude previous answers
// 3. get and set answer
const getShuffledElements = (difficulty) => {
    const shuffled = allLanguages.sort(() => 0.5 - Math.random()).slice(0, difficulty)
    const possibleAnswer = shuffled.filter(item => !previousAnswers.includes(item));
    roundAnswer = possibleAnswer[Math.floor(Math.random() * possibleAnswer.length)]
    previousAnswers.push(roundAnswer)
    // console.log(roundAnswer)
    setupAudio(roundAnswer.lang)
    return shuffled
}

// add in the characters and rules for clicking the characters
const newRound = (shuffled) => {
    roundCountValue ++
    const characters = shuffled
    gameSelection.innerHTML = ''
    selectedCharacter = ''
    characters.forEach(character => {
        gameSelection.innerHTML += `
        <div class="col-4 animate__animated animate__bounceInUp">
            <div class="image-holder option">
                <div class="text-center">
                    <p>${character.lang}</p>
                </div>
                <button class="btn btn-confirm d-none">Confirm</button>
                <img src="${character.img}" alt="">
            </div>
        </div>
        `
    })
    const options = gameSelection.querySelectorAll('.option')
    setConfirmButtons(options)
    timerBox.classList = 'col-2 d-none justify-content-center align-items-center animate__animated animate__fadeIn'
}

// function to control the display of green buttons on options when they are selected
function setConfirmButtons(options) {
    options.forEach(option => (
        option.addEventListener('click', () => {
            removeFocus(options)
            option.classList.add('image-selected')
            const currentBtn = option.querySelector('button')
            currentBtn.classList.remove('d-none')
            currentBtn.addEventListener('click', () => {
                currentBtn.disabled = true
                let sound = document.querySelector('#general')
                sound.currentTime = 0;
                sound.play()
                selectedCharacter = option.querySelector('p').innerText
                clearInterval(gameTimer)
                setTimeout(() => {endRound()}, 500)
            })
        })
    ))
}

// function to remove the green focus on elements which should no longer be in focus
function removeFocus (options) {
    options.forEach(option => {
        option.classList.remove('image-selected')
        option.querySelector('button').classList.add('d-none')
    })
}

// start the round by setting up and playing the audio
const setupAudio = (audioId) => {
    gameAudio = document.querySelector(`#${audioId.toLowerCase()}`)
    gameAudio.currentTime = 0;
    gameAudio.play()
    // !temporarily fast
    // gameAudio.playbackRate = 3
    gameAudioListener()
    gameProgressBar.style.width = 0;
    spans.forEach(span => {span.classList.add('playing')})
}

// listen to the audio for the progress bar and the ended event -> to move to next phase
const gameAudioListener = () => {
    gameAudio.addEventListener('timeupdate', (e) => {
        timeUpdateAudio(e)
    })
    gameAudio.addEventListener('ended', (e) => {
        audioEnded(e)
    })
}
// set the progress bar length
function timeUpdateAudio(e) {
    let total = e.target.duration
    let current = e.target.currentTime
    gameProgressBar.style.width = Math.round(current/total*100) + '%'
}

// begin the choice part of the game by
// 1. changing which div is showing (audioBox-off, gameplayBox - on) + the timer box
// 2. remove all audio listeners
// 3. start the countdown timer for the game choice
function audioEnded (e) {
    spans.forEach(span => {span.classList.remove('playing')})
    audioBox.classList.add('d-none')
    gameplayBox.classList.remove('d-none')
    timerBox.classList.remove('d-none')
    timerBox.classList.add('d-flex')
    gameAudio.removeEventListener('timeupdate', timeUpdateAudio)
    gameAudio.removeEventListener('ended', audioEnded)
    roundClock()
}

// function for the timer
const roundClock = () => {
    remainingTime = 14
    gameTimer = setInterval(() => {
       timer.innerText = remainingTime + 's'
       remainingTime --
       checkRemainingTime(remainingTime)
   }, 1000)
}

function checkRemainingTime(remainingTime) {
    if(remainingTime < 5  && remainingTime > 2){
        timerBox.classList = 'col-2 d-flex justify-content-center align-items-center orangeBox'
   } else if (remainingTime <= 2  && remainingTime >= 0) {
        timerBox.classList = 'col-2 d-flex justify-content-center align-items-center animate__animated animate__pulse animate__infinite redBox'
    } else if (remainingTime <= -1) {
        timerBox.classList = 'col-2 d-none justify-content-center align-items-center animate__animated animate__fadeIn'
        clearInterval(gameTimer)
        endRound()
   }
}

// At the end of the round, the game div should be replaced by result
// 1. game interval timer (the 15s countdown) needs to be cleared and set to null
// 2. a record of answers is kept
const endRound = () => {
    container04.classList.add('d-none')
    container05.classList.remove('d-none')
    clearInterval(gameTimer)
    gameTimer = null
    remainingTime = 15
    timer.innerText = 15 + 's'
    if(selectedCharacter === roundAnswer.lang){
        correct.classList.remove('d-none')
        score += 25
        actualWinnings.push(1)
    } else if (!selectedCharacter){
        timeout.classList.remove('d-none')
        actualWinnings.push(0)
    } else {
        incorrect.classList.remove('d-none')
        actualWinnings.push(0)
    }
    correctAnswer.forEach(answer => {
        answer.innerText = roundAnswer.lang
    })
    yourScore.innerText = score + '/100'
}

// Function which get's hit upon the user clicking "next" after one round has eneded
const nextRound = (button) => {
    setTimeout(() => {
        container05.classList.add('d-none')
        button.disabled = false
        audioBox.classList.remove('d-none')
        correct.classList.add('d-none')
        incorrect.classList.add('d-none')
        timeout.classList.add('d-none')
        gameplayBox.classList.add('d-none')
        timerBox.classList.add('d-none')
        timerBox.classList.remove('d-flex')
        clearInterval(gameTimer)
        if(roundCountValue === 4){
            roundCountValue = 0
            roundCount.innerText = 0 + '/4'
            container06.classList.remove('d-none')
            setResults()
        } else {
            container04.classList.remove('d-none')
            newRound(getShuffledElements(difficulty))
            roundCount.innerText = roundCountValue + '/4'
        }
    }, 250)
}
