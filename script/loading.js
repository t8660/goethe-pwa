'use strict';

// audio variables
audioIndex = 0
audioLoaded = 0
currentLoadedValue = 0
audioFiles = [
    {
        label: 'click',
        url: 'assets/audio/click.mp3'
    },
    {
        label: 'general',
        url: 'assets/audio/general.mp3'
    },
    {
        label: 'bemba',
        url: 'assets/audio/Bemba.mp3'
    },
    {
        label: 'german',
        url: 'assets/audio/Deutsch.mp3'
    },
    {
        label: 'isixhosa',
        url: 'assets/audio/isiXhosa.mp3'
    },
    {
        label: 'isizulu',
        url: 'assets/audio/isiZulu.mp3'
    },
    {
        label: 'russian',
        url: 'assets/audio/Russian.mp3'
    },
    {
        label: 'setswana',
        url: 'assets/audio/SetSwana.mp3'
    },
    {
        label: 'shona',
        url: 'assets/audio/Shona.mp3'
    },
    {
        label: 'turkish',
        url: 'assets/audio/Turkish.mp3'
    },
    {
        label: 'english',
        url: 'assets/audio/English.mp3'
    },
    {
        label: 'afrikaans',
        url: 'assets/audio/Afrikaans.mp3'
    },
    {
        label: 'results-sound',
        url: 'assets/audio/tralala.mp3'
    }
]

// image variables
imageFiles = [
    'assets/images/minions/xhosa.png',
    'assets/images/minions/afrikaans.png',
    'assets/images/minions/bemba.png',
    'assets/images/minions/english.png',
    'assets/images/minions/german.png',
    'assets/images/minions/isizulu.png',
    'assets/images/minions/russian.png',
    'assets/images/minions/setswana.png',
    'assets/images/minions/shona.png',
    'assets/images/minions/turkish.png',
    'assets/images/results/astronaut.png',
    'assets/images/results/cold-water.png',
    'assets/images/results/hiking.png',
    'assets/images/results/piano.png',
    'assets/images/results/pie.png'
]
imageIndex = 0
imageLoaded = 0;


// * we start preloading all the files
preloadAudio(audioFiles[0]);
preloadImage(imageFiles[0])

// * preloading functions
function preloadAudio(object) {
    var audio = new Audio()
    // once this file loads, it will call loadedAudio()
    audio.addEventListener('canplaythrough', loadedAudio, false)
    audio.src = object.url
    createEl ('audio', object.url, object.label)
}
function preloadImage(url) {
    var image = new Image()
    // once this file loads, it will call loadedImage()
    image.onload = loadedImage(url)
    image.src = url
}

// *  add the element (audio/image) to the dom
function createEl (type, url, label){
        var sound = document.createElement('audio')
        sound.src = url
        sound.type = 'audio/mpeg'
        sound.id = label
        tempAudioContainer.appendChild(sound)
}

// * once the file is loaded what to do
function loadedAudio() {
    // this will be called every time an audio file is loaded
    // we keep track of the loaded files vs the requested files
    audioLoaded++;
    audioIndex++
    counter()
    if (audioLoaded === audioFiles.length){
    	init();
    } else {
        preloadAudio(audioFiles[audioIndex]);
    }
}
function loadedImage(url ) {
    // this will be called every time an audio file is loaded
    // we keep track of the loaded files vs the requested files
    imageLoaded++;
    imageIndex++
    counter()
    if (imageLoaded === imageFiles.length){
    	// all have loaded
    	init();
    } else {
        preloadImage(imageFiles[imageIndex]);
    }
}

// * function to control the count up
function counter () {
    let countUp
    clearInterval(countUp)
    let amountLoaded = Math.floor(audioLoaded/audioFiles.length *50 + imageLoaded/imageFiles.length * 50)
    countUp = setInterval(() => {
        if(currentLoadedValue >= amountLoaded){
            clearInterval(countUp)
        }
        loadedPercentage.innerText = currentLoadedValue + '%'
        currentLoadedValue ++
    }, 100)
}

// * function to check whether both images and audio have loaded and reveal container 01
function init() {
    if(audioLoaded === audioFiles.length && imageLoaded === imageFiles.length){
        container00.remove()
        container01.classList.remove('d-none')
    }
    
}



