function setResults() {
    actualWinnings.forEach((score, index) => {
        let sound = document.querySelector('#results-sound')
        sound.currentTime = 0;
        sound.play()
        setTimeout(() => {
            ears[index].classList.remove('d-none')
            if(score){
                ears[index].classList.add('green')
            } 
        }, (1 + index) * 600)
    })
    let thisAnswer
    if(score){
        thisAnswer = answers[score/25]
    } else {
        thisAnswer = answers[0]
    }
    banner.src = `assets/images/results/${thisAnswer.image}`
    heading.innerText = thisAnswer.heading
    subHeading.innerText = thisAnswer.subheading
}