'use strict';


// loading screen constants
const container00 = document.querySelector('#container-00')
const loadedPercentage = container00.querySelector('#loaded-percentage')
const tempContainer = document.querySelector('#temp-container')
const tempAudioContainer = document.querySelector('#temp-audio-container')
const tempImgContainer = document.querySelector('#temp-img-container')
let audioFiles, imageFiles, audioIndex, audioLoaded, currentLoadedValue, imageIndex, imageLoaded

// welcome screen constants
const container01 = document.querySelector('#container-01')

// the rules of the game
const container02Part1 = document.querySelector('#container-02-part-1')
const container02Part2 = document.querySelector('#container-02-part-2')
const skipAudioBtn = document.querySelector('#skip-audio')
const skipText = skipAudioBtn.querySelector('p')
const rulesProgressBar = document.querySelector('#rules-progress-bar .progression')
let englishAudio
const hiddenAudioEls = document.querySelectorAll('.hidden-audio')
const placeholderBtn = document.querySelector('#playPlaceholderBtn')
const playBtnDiv = document.querySelector('#playBtnDiv')

// difficulty constants
const container03 = document.querySelector('#container-03')
const diffOptions = document.querySelectorAll('.difficulty')
const diffGoBtn = document.querySelector('.go-btn-difficulty')


//
let difficulty = ''

// game play
let firstPlay = false
const container04 = document.querySelector('#container-04')
const gameProgressBar = document.querySelector('#game-progress-bar .progression')
const gameplayBox = document.querySelector('.gameplay')
const audioBox = document.querySelector('#audio')
const gameSelection = document.querySelector('.gameSelection')
const spans = document.querySelectorAll('#audio .loader span')
const timerBox = document.querySelector('#timer')
const timer = document.querySelector('.timer')
const roundCount = document.querySelector('#round-count p.bigger')
const allLanguages = [
    {
        'lang' : 'Afrikaans', 
        'img' : 'assets/images/minions/afrikaans.png'
    },
    {
        'lang' : 'Bemba', 
        'img' : 'assets/images/minions/bemba.png'
    },
    {
        'lang' : 'German', 
        'img' : 'assets/images/minions/german.png'
    },
    {
        'lang' : 'IsiZulu', 
        'img' : 'assets/images/minions/isizulu.png'
    },
    {
        'lang' : 'Russian', 
        'img' : 'assets/images/minions/russian.png'
    },
    {
        'lang' : 'Setswana', 
        'img' : 'assets/images/minions/setswana.png'
    },
    {
        'lang' : 'Shona', 
        'img' : 'assets/images/minions/shona.png'
    },
    {
        'lang' : 'Turkish', 
        'img' : 'assets/images/minions/turkish.png'
    },
    {
        'lang' : 'IsiXhosa', 
        'img' : 'assets/images/minions/xhosa.png'
    }
]
let previousAnswers = []
let roundAnswer
let selectedCharacter
let roundCountValue = 0
let score = 0
let remainingTime = 15
let watchGameAudioTime
let gameTimer
let actualWinnings = []
let playing = false;
let gameAudio

// answers
const container05 = document.querySelector('#container-05')
const correct = document.querySelector('.correct')
const incorrect = document.querySelector('.incorrect')
const timeout = document.querySelector('.timeout')
const yourScore = document.querySelector('#your-score')
const correctAnswer = document.querySelectorAll('.correct-answer')

// results
const container06 = document.querySelector('#container-06')
const earsBox = document.querySelector('.ear-holder')
const ears = document.querySelectorAll('.result-button')
const banner = document.querySelector('#banner')
const heading = document.querySelector('#heading')
const subHeading = document.querySelector('#sub-heading')

const answers = [
    {
        'image': 'astronaut.png',
        'heading': 'Have you visited the moon recently?',
        'subheading': 'Cause languages seem to be a little foreign.'
    },
    {
        'image': 'hiking.png',
        'heading': 'The destination is far away, but at least…',
        'subheading': "you’ve taken the first step."
    },
    {
        'image': 'cold-water.png',
        'heading': 'Your language cup is half full…',
        'subheading': "Or half empty?"
    },
    {
        'image': 'pie.png',
        'heading': "If languages were a pie you’d be stuffed.",
        'subheading' : ''
    },
    {
        'image': 'piano.png',
        'heading': 'You must be a piano,',
        'subheading': "cause you’ve been tuned to perfection"
    }
]

// reset button
const resetBtn = document.querySelector('#reset')