'use strict'

const linkBtns = document.querySelectorAll('button')


linkBtns.forEach(btn => {
    btn.addEventListener('click', (e) => {
        btn.disabled = true
        setTimeout(() => {btn.disabled = false}, 250)
        let time = 250
        if(btn.dataset.time){ time = btn.dataset.time}
        if(btn.dataset.sound){
            let sound = document.querySelector(`#${btn.dataset.sound}`)
            sound.currentTime = 0;
            sound.play()
        }
        if(btn.dataset.hideId){
            setTimeout(() => {
                document.querySelector(`#${btn.dataset.hideId}`).classList.add('d-none')
                document.querySelector(`#${btn.dataset.showId}`).classList.remove('d-none')
            }, time)
        }
    })
})


